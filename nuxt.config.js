module.exports = {
  mode: 'spa',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ]
  },
  generate: {
    fallback: true,
    routes: [
      '/tips/1',
      '/tips/2',
      '/tips/3',
      '/tips/4',
      '/tips/5',
      '/tips/6'
    ]
  },
  css: [
    '@/assets/scss/style.scss'
  ],
  modules: [
    '@nuxtjs/pwa',
  ],
  pwa: {
    manifest: {
      name: '減災教室',
      title: '減災教室',
      lang: 'ja',
      scope: '/',
      start_url: '/category'
    }
  }
}